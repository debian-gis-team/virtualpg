/* 
/ virtualpg.h
/
/ public declarations
/
/ version  2.0.0-RC0, 2018 July 14
/
/ Author: Sandro Furieri a.furieri@lqt.it
/
/ ------------------------------------------------------------------------------
/ 
/ Version: MPL 1.1/GPL 2.0/LGPL 2.1
/ 
/ The contents of this file are subject to the Mozilla Public License Version
/ 1.1 (the "License"); you may not use this file except in compliance with
/ the License. You may obtain a copy of the License at
/ http://www.mozilla.org/MPL/
/ 
/ Software distributed under the License is distributed on an "AS IS" basis,
/ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
/ for the specific language governing rights and limitations under the
/ License.
/
/ The Original Code is the VirtualPG library
/
/ The Initial Developer of the Original Code is Alessandro Furieri
/ 
/ Portions created by the Initial Developer are Copyright (C) 2013-2018
/ the Initial Developer. All Rights Reserved.
/ 
/ Contributor(s):
/
/ Alternatively, the contents of this file may be used under the terms of
/ either the GNU General Public License Version 2 or later (the "GPL"), or
/ the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
/ in which case the provisions of the GPL or the LGPL are applicable instead
/ of those above. If you wish to allow use of your version of this file only
/ under the terms of either the GPL or the LGPL, and not to allow others to
/ use your version of this file under the terms of the MPL, indicate your
/ decision by deleting the provisions above and replace them with the notice
/ and other provisions required by the GPL or the LGPL. If you do not delete
/ the provisions above, a recipient may use your version of this file under
/ the terms of any one of the MPL, the GPL or the LGPL.
/ 
*/

/**
 \file virtualpg.h 
 
 Function declarations and constants for VirtualPG library
 */
#ifndef DOXYGEN_SHOULD_SKIP_THIS
#ifdef DLL_EXPORT
#define VIRTUALPG_DECLARE __declspec(dllexport)
#else
#define VIRTUALPG_DECLARE extern
#endif
#endif

#ifndef _VIRTUALPG_H
#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define _VIRTUALPG_H
#endif

#include <libpq-fe.h>

#ifdef __cplusplus
extern "C"
{
#endif

	/**
	 virtualized libPQ methods

	 \sa virtualPQptr
	 */
    typedef struct virtualPQstruct
    {
	/** pointer to PQclear */
	void (*PQclear) (PGresult * res);
	/** pointer to PQconnectdb */
	PGconn *(*PQconnectdb) (const char *conninfo);
	/** pointer to PQerrorMessage */
	char *(*PQerrorMessage) (const PGconn * conn);
	/** pointer to PQexec */
	PGresult *(*PQexec) (PGconn * conn, const char *command);
	/** pointer to PQfinish */
	void (*PQfinish) (PGconn * conn);
	/** pointer to PQgetisnull */
	int (*PQgetisnull) (const PGresult * res, int row_number,
			    int column_number);
	/** pointer to PQgetvalue */
	char *(*PQgetvalue) (const PGresult * res, int row_number,
			     int column_number);
	/** pointer to PQlibVersion */
	int (*PQlibVersion) (void);
	/** pointer to PQnfields */
	int (*PQnfields) (const PGresult * res);
	/** pointer to PQntuples */
	int (*PQntuples) (const PGresult * res);
	/** pointer to PQresultStatus */
	  ExecStatusType (*PQresultStatus) (const PGresult * res);
	/** pointer to PQstatus */
	  ConnStatusType (*PQstatus) (const PGconn * conn);
    } virtualPQ;
	/**
	 Typedef for virtual libPQ structure

	 \sa virtualPQ
	 */
    typedef virtualPQ *virtualPQptr;

    /** virtual implementation for LibPQ - PQclear */
    VIRTUALPG_DECLARE void vpgPQclear (PGresult * res);
    /** virtual implementation for LibPQ - PQconnectdb */
    VIRTUALPG_DECLARE PGconn *vpgPQconnectdb (const char *conninfo);
    /** virtual implementation for LibPQ - PQerrorMessage */
    VIRTUALPG_DECLARE char *vpgPQerrorMessage (const PGconn * conn);
    /** virtual implementation for LibPQ - PQexec */
    VIRTUALPG_DECLARE PGresult *vpgPQexec (PGconn * conn, const char *command);
    /** virtual implementation for LibPQ - PQfinish */
    VIRTUALPG_DECLARE void vpgPQfinish (PGconn * conn);
    /** virtual implementation for LibPQ - PQgetisnull */
    VIRTUALPG_DECLARE int vpgPQgetisnull (const PGresult * res, int row_number,
					  int column_number);
    /** virtual implementation for LibPQ - PQgetvalue */
    VIRTUALPG_DECLARE char *vpgPQgetvalue (const PGresult * res, int row_number,
					   int column_number);
    /** virtual implementation for LibPQ - PQlibVersion */
    VIRTUALPG_DECLARE int vpgPQlibVersion(void);
    /** virtual implementation for LibPQ - PQnfields */
    VIRTUALPG_DECLARE int vpgPQnfields (const PGresult * res);
    /** virtual implementation for LibPQ - PQntuples */
    VIRTUALPG_DECLARE int vpgPQntuples (const PGresult * res);
    /** virtual implementation for LibPQ - PQresultStatus */
    VIRTUALPG_DECLARE ExecStatusType vpgPQresultStatus (const PGresult * res);
    /** virtual implementation for LibPQ - PQstatus */
    VIRTUALPG_DECLARE ConnStatusType vpgPQstatus (const PGconn * conn);

    /**
     Return the current library version.
     
     \return the version string.
     */
    VIRTUALPG_DECLARE const char *virtualpg_version (void);

    /**
     Initializes libvirtualpg as an extension to SQLite
     
     \param db_handle pointer to the current DB connection.
     \param virtual_api pointer to a virtualPQ struct.

     \return SQLITE_OK will be returned on success, otherwise any appropriate
     error code on failure.
     */
    VIRTUALPG_DECLARE int virtualpg_extension_init (sqlite3 * db_handle,
						    virtualPQptr virtual_api);

#ifdef __cplusplus
}
#endif

#endif				/* _VIRTUALPG_H */
